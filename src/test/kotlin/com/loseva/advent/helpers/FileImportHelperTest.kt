package com.loseva.advent.helpers

import org.testng.Assert.assertNotNull
import org.testng.annotations.Test

class FileImportHelperTest {

    @Test
    fun testReadListOfCommaSeparatedInts() {
        val result = readListOfCommaSeparatedInts("day02/input.txt")
        assertNotNull(result)
    }
}