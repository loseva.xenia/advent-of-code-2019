package com.loseva.advent.day.four

import org.testng.Assert.*
import org.testng.annotations.Test

class PasswordGuesserKtTest {

    @Test
    fun testGuessPasswords() {
        val result = guessPasswords(265275, 781584)
        println("result: $result")

        assertEquals(result, 626)
    }

    @Test
    fun testDoesNumberMeetCriteriaCorrectNumber() {
        val result = doesNumberMeetCriteria(112233)
        assertTrue(result)
    }

    @Test
    fun testDoesNumberMeetCriteriaWrongNumberDecreases() {
        val result = doesNumberMeetCriteria(111122)
        assertTrue(result)
    }

    @Test
    fun testDoesNumberMeetCriteriaWrongNumberNoAdjacent() {
        val result = doesNumberMeetCriteria(123444)
        assertFalse(result)
    }
}