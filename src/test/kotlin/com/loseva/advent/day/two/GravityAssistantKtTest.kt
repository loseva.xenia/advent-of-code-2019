package com.loseva.advent.day.two

import com.loseva.advent.helpers.readListOfCommaSeparatedInts
import org.testng.Assert.assertEquals
import org.testng.annotations.Test

class GravityAssistantKtTest {

    @Test
    fun testRestoreGravityAssistProgram() {
    }

    @Test
    fun testExecuteProgram() {
    }

    @Test
    fun testGravityAssistant() {
        var list = readListOfCommaSeparatedInts("day02/input.txt")
        list = restoreGravityAssistProgram(list)
        val result = executeProgram(list)
        println(result)
        assertEquals(result, 9706670)
    }

    @Test
    fun testCorrectOutput() {
        var combination = -3
        for (noun in 0..99) {
            for (verb in 0..99) {
                var programList = readListOfCommaSeparatedInts("day02/input.txt")
                restoreGravityAssistProgram(programList, noun, verb)
                val result = executeProgram(programList)
                if (result == 19690720) {
                    combination = 100 * noun + verb
                }
            }
        }

        assertEquals(combination, 2552)
    }
}