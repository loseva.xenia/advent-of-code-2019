package com.loseva.advent.day.one

import com.loseva.advent.helpers.readListOfIntsFromFile
import org.testng.Assert.assertEquals
import org.testng.annotations.Test

class FuelCalculatorTest {

    @Test
    fun testCalculateFuelForModuleIfMassIsDividedBy3() {
        val result = calculateFuelForModule(12)
        assertEquals(result, 2)
    }

    @Test
    fun testCalculateFuelForModuleIfMassNotDividedBy3() {
        val result = calculateFuelForModule(14)
        assertEquals(result, 2)
    }

    @Test
    fun testSumFuelForAllModules() {
        val result = sumFuelForAllModules(listOf(12, 13, 15))
        assertEquals(result, 7)
    }

    @Test
    fun testResolveAdventProblem() {
        val array = readListOfIntsFromFile("day01/input.txt")
        val result = sumFuelForAllModules(array)
        assertEquals(result, 3363760)
    }

    @Test
    fun testCalculateFuelForModuleAndFuelWhenFuelIsZeroish() {
        val result = calculateFuelForModuleAndFuel(14)
        assertEquals(result, 2)
    }

    @Test
    fun testCalculateFuelForModuleAndFuelWhenFuelIsNonZeroish() {
        val result = calculateFuelForModuleAndFuel(1969)
        assertEquals(result, 966)
    }

    @Test
    fun testSumFuelForAllModulesAndFuels() {
        val result = sumFuelForAllModulesAndFuels(listOf(14, 1969, 100756))
        assertEquals(result, 51314)
    }

    @Test
    fun testResolveAdventProblemForDayTwo() {
        val array = readListOfIntsFromFile("day01/input.txt")
        val result = sumFuelForAllModulesAndFuels(array)
        println(result)
        assertEquals(result, 5042767)
    }
}