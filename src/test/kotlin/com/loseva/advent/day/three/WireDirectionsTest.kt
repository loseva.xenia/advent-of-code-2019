package com.loseva.advent.day.three

import com.loseva.advent.helpers.readWiresInput
import org.testng.annotations.Test
import java.awt.Point
import kotlin.math.abs

class GravityAssistantKtTest {

    @Test
    fun testFindWireDirections() {
        val wires = readWiresInput("day03/input.txt")
        val wireOne = ArrayList<WireDirection>()
        val wireTwo = ArrayList<WireDirection>()

        val directionsOne = wires[0].split(",")
        directionsOne.forEach { wireOne.add(WireDirection(it.substring(0, 1), it.substring(1).toInt())) }

        val directionsTwo = wires[1].split(",")
        directionsTwo.forEach { wireTwo.add(WireDirection(it.substring(0, 1), it.substring(1).toInt())) }

        val pointsListOne = HashSet<Point>()
        val pointsListTwo = HashSet<Point>()

        var previousPoint = Point(0, 0)
        wireOne.forEach {
            for (i in 1..it.offset) {
                val newPoint = Point(previousPoint.x + it.direction.x, previousPoint.y + it.direction.y)
                pointsListOne.add(newPoint)
                previousPoint = newPoint
            }
        }

        previousPoint = Point(0, 0)
        wireTwo.forEach {
            for (i in 1..it.offset) {
                val newPoint = Point(previousPoint.x + it.direction.x, previousPoint.y + it.direction.y)
                pointsListTwo.add(newPoint)
                previousPoint = newPoint
            }
        }

        val intersections = pointsListOne.filter {
            pointsListTwo.contains(it)
        }

        var closestIntersection = intersections[0]
        intersections.forEach {
            if (getDistance(closestIntersection) > getDistance(it))
                closestIntersection = it
        }

        println(closestIntersection)
        println(getDistance(closestIntersection))

    }

    @Test
    fun testFindWireIntersection() {

        val wires = readWiresInput("day03/input.txt")
        val wireOne = ArrayList<WireDirection>()
        val wireTwo = ArrayList<WireDirection>()

        val directionsOne = wires[0].split(",")
        directionsOne.forEach { wireOne.add(WireDirection(it.substring(0, 1), it.substring(1).toInt())) }

        val directionsTwo = wires[1].split(",")
        directionsTwo.forEach { wireTwo.add(WireDirection(it.substring(0, 1), it.substring(1).toInt())) }

        val pointsListOne = HashMap<Point, Int>()
        val pointsListTwo = HashMap<Point, Int>()

        var previousPoint = Point(0, 0)
        var step = 0
        wireOne.forEach {
            for (i in 1..it.offset) {
                step += 1
                val newPoint = Point(previousPoint.x + it.direction.x, previousPoint.y + it.direction.y)
                pointsListOne[newPoint] = step
                previousPoint = newPoint
            }
        }

        previousPoint = Point(0, 0)
        step = 0
        wireTwo.forEach {
            for (i in 1..it.offset) {
                step += 1
                val newPoint = Point(previousPoint.x + it.direction.x, previousPoint.y + it.direction.y)
                pointsListTwo[newPoint] = step
                previousPoint = newPoint
            }
        }

        val intersections = pointsListOne.filter { (key, value) -> pointsListTwo.contains(key) }.keys.toList()

        var closestIntersection = pointsListOne[intersections[0]]!! + (pointsListTwo[intersections[0]]!!)

        intersections.forEach{
            if (closestIntersection > pointsListOne[it]!! + pointsListTwo[it]!!)
                closestIntersection = pointsListOne[it]!! + pointsListTwo[it]!!
        }

        println(closestIntersection)

    }

    fun getDistance(point: Point): Int {
        return abs(point.x) + abs(point.y)
    }
}