package com.loseva.advent.day.five

import com.loseva.advent.helpers.readListOfCommaSeparatedInts
import org.testng.Assert.assertEquals
import org.testng.annotations.Test

class ConditionerAssistantKtTest {

    @Test
    fun testRunDiagnostics() {
        var list = readListOfCommaSeparatedInts("day05/input.txt")
        val result = runDiagnostics(list, 5)

        assertEquals(result, 14340395)
    }
}