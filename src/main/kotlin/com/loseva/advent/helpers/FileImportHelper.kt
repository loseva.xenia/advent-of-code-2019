package com.loseva.advent.helpers

import java.io.File

fun readListOfIntsFromFile(fileName: String): List<Int> {
    val ints: ArrayList<Int> = ArrayList()
    File(fileName).forEachLine { ints.add(it.toInt()) }
    return ints
}

fun readListOfCommaSeparatedInts(fileName: String): MutableList<Int> {
    val ints: ArrayList<Int> = ArrayList()
    File(fileName).readText().split(",").forEach { ints.add(it.toInt()) }
    return ints
}

fun readWiresInput(fileName: String): List<String> {
    val wires: ArrayList<String> = ArrayList()
    File(fileName).forEachLine { wires.add(it) }
    return wires
}