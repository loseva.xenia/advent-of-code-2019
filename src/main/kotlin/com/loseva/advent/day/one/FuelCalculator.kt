package com.loseva.advent.day.one

fun calculateFuelForModule(moduleMass: Int): Int {
    return moduleMass / 3 - 2
}

fun sumFuelForAllModules(modulesList: List<Int>): Int {
    var sum = 0
    modulesList.forEach { sum += calculateFuelForModule(it) }
    return sum
}

fun calculateFuelForModuleAndFuel(moduleMass: Int): Int {
    var resultingMass = 0
    var mass = calculateFuelForModule(moduleMass)
    while (mass / 3 > 0) {
        resultingMass += mass
        mass = calculateFuelForModule(mass)
    }
    if (mass > 0) {
        resultingMass += mass
    }
    return resultingMass
}

fun sumFuelForAllModulesAndFuels(modulesList: List<Int>): Int {
    var sum = 0
    modulesList.forEach { sum += calculateFuelForModuleAndFuel(it) }
    return sum
}