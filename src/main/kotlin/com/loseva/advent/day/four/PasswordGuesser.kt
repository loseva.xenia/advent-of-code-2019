package com.loseva.advent.day.four

fun guessPasswords(rangeStart: Int, rangeEnd: Int): Int {
    var counter = 0
    for (i in rangeStart..rangeEnd) {
        if (doesNumberMeetCriteria(i)) {
            println(i)
            counter++
        }
    }

    return counter
}

fun doesNumberMeetCriteria(number: Int): Boolean {
    var testNumber = number

    var adjasentNumbers = false
    var adjN: Int? = null

    var repN: Int? = null

    while (testNumber > 0) {
        val lastNumber = testNumber % 10
        testNumber /= 10
        val nextNumber = testNumber % 10

        if (lastNumber < nextNumber) {
            return false
        }

        if (!adjasentNumbers) {
            if (lastNumber == nextNumber) {
                adjN = lastNumber
                val nextNextNumber = testNumber / 10 % 10
                if (nextNumber == nextNextNumber) {
                    repN = lastNumber

                    if (repN == adjN) {
                        adjasentNumbers = false
                    }

                } else {
                    if (repN == null)
                        adjasentNumbers = true
                    else {
                        if (repN != adjN) {
                            adjasentNumbers = true
                        }
                    }
                }
            }
        }

    }
    return adjasentNumbers
}