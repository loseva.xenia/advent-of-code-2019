package com.loseva.advent.day.two

import com.loseva.advent.day.five.ParameterMode

class AddCommand(positionOne: Int, positionTwo: Int, positionResult: Int, resultList: MutableList<Int>, val paramOneMode: Int = 0, val paramTwoMode: Int = 0) :
    Command(positionOne, positionTwo, positionResult, resultList) {

    override fun action(): Int {
        val valOne = if(ParameterMode.POSITION.mode == paramOneMode) {
            resultList[positionOne]
        } else positionOne
        val valTwo = if (ParameterMode.POSITION.mode == paramTwoMode) {
            resultList[positionTwo!!]
        } else positionTwo!!
        resultList[positionResult!!] = valOne + valTwo

        return 4
    }
}