package com.loseva.advent.day.two

abstract class Command(positionOne: Int, positionTwo: Int?, positionResult: Int?, resultList: MutableList<Int>) {
    val positionOne = positionOne
    val positionTwo = positionTwo
    val positionResult = positionResult
    var resultList = resultList

    abstract fun action(): Int
}