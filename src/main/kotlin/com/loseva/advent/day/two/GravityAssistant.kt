package com.loseva.advent.day.two

fun restoreGravityAssistProgram(programList: MutableList<Int>): MutableList<Int> {
    return restoreGravityAssistProgram(programList, 12, 2)
}

fun restoreGravityAssistProgram(programList: MutableList<Int>, positionOne: Int, positionTwo: Int): MutableList<Int> {
    programList[1] = positionOne
    programList[2] = positionTwo

    return programList
}

fun executeProgram(programList: MutableList<Int>): Int {
    var position = 0
    while (position < programList.size) {
        val code = programList[position]
        var command: Command

        command = when (code) {
            Opcode.FINISH.code -> return programList[0]
            Opcode.ADD.code -> AddCommand(
                programList[position + 1],
                programList[position + 2],
                programList[position + 3],
                programList
            )
            Opcode.MULTIPLY.code -> MultiplyCommand(
                programList[position + 1],
                programList[position + 2],
                programList[position + 3],
                programList
            )
            else -> throw RuntimeException("No command for code $code!")
        }

        command.action()

        position += 4
    }

    throw RuntimeException("Could not find the end of the program!")

}

fun findCorrectOutput(programList: MutableList<Int>): Int {
    for (noun in 0..99) {
        for (verb in 0..99) {
            restoreGravityAssistProgram(programList)
            val result = executeProgram(programList)
            if (result == 19690720) {
                return 100 * noun + verb
            }
        }
    }

    throw RuntimeException("No output found!")
}