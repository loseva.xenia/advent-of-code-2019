package com.loseva.advent.day.five

enum class ParameterMode(val mode: Int) {
    POSITION(0),
    IMMEDIATE(1)
}