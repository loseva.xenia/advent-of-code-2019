package com.loseva.advent.day.five

import com.loseva.advent.day.two.Command

class LessThanCommand(
    paramOne: Int,
    paramTwo: Int,
    paramResult: Int,
    resultList: MutableList<Int>,
    val paramOneMode: Int = 0,
    val paramTwoMode: Int = 0
) : Command(paramOne, paramTwo, paramResult, resultList) {

    override fun action(): Int {
        val argOne = if (paramOneMode == ParameterMode.POSITION.mode) {
            resultList[positionOne]
        } else positionOne

        val argTwo = if (paramTwoMode == ParameterMode.POSITION.mode) {
            resultList[positionTwo!!]
        } else positionTwo!!

        resultList[positionResult!!] = if (argOne < argTwo) {
            1
        } else 0

        return 4
    }
}