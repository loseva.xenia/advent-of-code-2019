package com.loseva.advent.day.five

import com.loseva.advent.day.two.AddCommand
import com.loseva.advent.day.two.Command
import com.loseva.advent.day.two.MultiplyCommand
import com.loseva.advent.day.two.Opcode

var result: Int? = null

fun runDiagnostics(programList: MutableList<Int>, input: Int): Int {
    var position = 0
    while (position < programList.size) {
        val controlCommand = "%04d".format(programList[position])

        val code = controlCommand.takeLast(2).toInt()
        val argOneMode = controlCommand[1].toString().toInt()
        val argTwoMode = controlCommand[0].toString().toInt()
        var command: Command



        command = when (code) {
            Opcode.FINISH.code -> return result!!
            Opcode.ADD.code -> AddCommand(
                programList[position + 1],
                programList[position + 2],
                programList[position + 3],
                programList,
                argOneMode,
                argTwoMode
            )
            Opcode.MULTIPLY.code -> MultiplyCommand(
                programList[position + 1],
                programList[position + 2],
                programList[position + 3],
                programList,
                argOneMode,
                argTwoMode
            )
            Opcode.INPUT.code -> InputCommand(input, programList[position + 1], programList)
            Opcode.OUTPUT.code -> OutputCommand(programList[position + 1], programList, argOneMode)
            Opcode.JUMP_IF_TRUE.code -> JumpIfTrueCommand(
                programList[position + 1],
                programList[position + 2],
                programList,
                argOneMode,
                argTwoMode,
                position
            )
            Opcode.JUMP_IF_FALSE.code -> JumpIfFalseCommand(
                programList[position + 1],
                programList[position + 2],
                programList,
                argOneMode,
                argTwoMode,
                position
            )
            Opcode.LESS_THAN.code -> LessThanCommand(
                programList[position + 1],
                programList[position + 2],
                programList[position + 3],
                programList,
                argOneMode,
                argTwoMode
            )
            Opcode.EQUALS.code -> EqualCommand(
                programList[position + 1],
                programList[position + 2],
                programList[position + 3],
                programList,
                argOneMode,
                argTwoMode
            )
            else -> throw RuntimeException("No command for code $code!")
        }

        val step = command.action()

        if (command is OutputCommand) {
            result = command.result
        }

        position += step
    }

    throw RuntimeException("Could not find the result")
}