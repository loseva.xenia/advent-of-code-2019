package com.loseva.advent.day.five

import com.loseva.advent.day.two.Command

class OutputCommand(positionOne: Int, resultList: MutableList<Int>, val paramOneMode: Int = 0) :
    Command(positionOne, null, null, resultList) {

    var result: Int? = null

    override fun action(): Int {
        val valOne = if(ParameterMode.POSITION.mode == paramOneMode) {
            resultList[positionOne]
        } else positionOne
        println(valOne)

        result = valOne

        return 2
    }
}