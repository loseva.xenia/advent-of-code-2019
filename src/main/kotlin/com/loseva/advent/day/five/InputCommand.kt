package com.loseva.advent.day.five

import com.loseva.advent.day.two.Command

class InputCommand(private val input: Int, positionOne: Int, resultList: MutableList<Int>) :
    Command(positionOne, null, null, resultList) {

    override fun action(): Int {
        val valOne = resultList[positionOne]
        resultList[positionOne] = input

        return 2
    }
}