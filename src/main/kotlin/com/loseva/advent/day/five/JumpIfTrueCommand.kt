package com.loseva.advent.day.five

import com.loseva.advent.day.two.Command

class JumpIfTrueCommand(
    positionOne: Int,
    positionTwo: Int,
    resultList: MutableList<Int>,
    val argOneParameter: Int = 0,
    val argTwoParameter: Int = 0,
    val pointerPosition: Int
) :
    Command(positionOne, positionTwo, null, resultList) {


    override fun action(): Int {
        val argOne = if (argOneParameter == ParameterMode.POSITION.mode) {
            resultList[positionOne]
        } else {
            positionOne
        }

        val argTwo = if (argTwoParameter == ParameterMode.POSITION.mode) {
            resultList[positionTwo!!]
        } else {
            positionTwo
        }

        return if (argOne != 0) {
            return argTwo!! - pointerPosition
        } else 3
    }

}