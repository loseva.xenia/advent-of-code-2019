package com.loseva.advent.day.three

import java.awt.Point

class WireDirection(direction: String, offset: Int) {

    val direction = resolveDirection(direction)

    val offset = offset

    private fun resolveDirection(direction: String): Point {
        return when (direction) {
            "L" -> Point(-1, 0)
            "R" -> Point(1, 0)
            "U" -> Point(0, 1)
            "D" -> Point(0, -1)
            else -> throw RuntimeException("Could not resolve direction for $direction!")
        }
    }
}